from io import FileIO, BytesIO
from typing import NewType, Union, Iterable, Callable, Any, Tuple


FourBytes = NewType('FourBytes', bytes)
ByteStream = NewType('ByteStream', Union[FileIO, BytesIO])
RGB = NewType('RGB', Tuple[int, int, int])
RGBA = NewType('RGBA', Tuple[int, int, int, int])


class ChunkName:
    def __init__(self, name: FourBytes, is_ancillary: bool, is_private: bool,
                 is_reserved: bool, is_safe2copy: bool):
        self.name = name
        self.is_ancillary = is_ancillary
        self.is_private = is_private
        self.is_reserved = is_reserved
        self.is_safe2copy = is_safe2copy

    @staticmethod
    def from_bytes(name: FourBytes) -> 'ChunkName':
        flags = map(has_fifth_bit, name)
        return ChunkName(name, *flags)


class PngChunk:
    def __init__(self, length: int, chunk_name: ChunkName,
                 data: bytes, crc: int):
        self.length = length
        self.chunk_name = chunk_name
        self.data = data
        self.crc = crc


class PngFormatError(Exception):
    pass


class PngChunkError(Exception):
    pass


def has_fifth_bit(char: int) -> bool:
    return bool(char & (1 << 5))


def strict_read(bs: ByteStream, amount: int, allow_empty: bool = False) -> bytes:
    result = bs.read(amount)
    if allow_empty and not result:
        return result
    if len(result) != amount:
        raise PngFormatError(f'Expected to read {amount} bytes, but met EOF')
    return result


def contains(iterable: Iterable) -> Callable[[Any], bool]:
    return lambda x: x in iterable


def is_positive(num: int) -> bool:
    return num > 0


def check_many(values, names, validators) -> None:
    for val, name, is_valid in zip(values, names, validators):
        if not is_valid(val):
            raise PngChunkError(f'Invalid {name}: {val}')

