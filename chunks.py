from png_helper import *
from typing import Tuple, Optional
from struct import unpack
from enum import IntEnum
from bitstring import ConstBitArray
from zlib import decompress


class Unit(IntEnum):
    Unknown = 0
    Meter = 1


class ColorType(IntEnum):
    Grayscale = 0
    Rgb = 2
    Palette = 3
    GrayscaleAlpha = 4
    Rgba = 6


class RenderingIntent(IntEnum):
    Perceptual = 0
    RelativeColorimetric = 1
    Saturation = 2
    AbsoluteColorimetric = 3


class InterlaceMethod(IntEnum):
    NoInterlace = 0
    Adam7 = 1


class FilterMethod(IntEnum):
    NoFilter = 0
    Sub = 1
    Up = 2
    Average = 3
    Paeth = 4


class IhdrChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, width: int, height: int,
                 bit_depth: int, color_type: ColorType, compression_method: int,
                 filter_method: FilterMethod, interlace_method: InterlaceMethod):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.width = width
        self.height = height
        self.bit_depth = bit_depth
        self.color_type = color_type
        self.compression_method = compression_method
        self.filter_method = filter_method
        self.interlace_method = interlace_method


class PlteChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, palette: Tuple[RGB, ...]):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.palette = palette


class IendChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)


class PhysChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, pixels_per_unit: Tuple[int, int], unit: Unit):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.unit = unit
        self.pixels_per_unit = pixels_per_unit


class SrgbChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, rendering_intent: RenderingIntent):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.rendering_intent = rendering_intent


class BkgdChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, background: Any):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.background = background


class TimeChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, time: str):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.time = time


class TrnsChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, transparency: Any):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.transparency = transparency


class GamaChunk(PngChunk):
    def __init__(self, png_chunk: PngChunk, gamma: int):
        super().__init__(png_chunk.length, png_chunk.chunk_name, png_chunk.data, png_chunk.crc)
        self.gamma = gamma


def get_gama_chunk(png_chunk: PngChunk) -> Optional[GamaChunk]:
    if png_chunk.chunk_name.name != b'gAMA':
        return None
    g, *_ = unpack('>I', png_chunk.data)
    return GamaChunk(png_chunk, g)


def get_trns_chunk(png_chunk: PngChunk, ihdr: IhdrChunk) -> Optional[TrnsChunk]:
    if png_chunk.chunk_name.name != b'tRNS':
        return None
    if ihdr.color_type == ColorType.Palette:
        alphas = tuple(png_chunk.data)
        return TrnsChunk(png_chunk, alphas)
    elif ihdr.color_type == ColorType.Grayscale:
        alpha, *_ = unpack('>H', png_chunk.data)
        return TrnsChunk(png_chunk, alpha)
    elif ihdr.color_type == ColorType.Rgb:
        r, g, b = unpack('>HHH', png_chunk.data)
        return TrnsChunk(png_chunk, (r, g, b))
    else:
        raise PngChunkError(f'TRNS chunk is supported only for RGB, Grayscale and indexed color')


def get_time_chunk(png_chunk: PngChunk) -> Optional[TimeChunk]:
    if png_chunk.chunk_name.name != b'tIME':
        return None
    time = '%.04d-%.02d-%.02d %.02d:%.02d:%.02d' % unpack('>Hbbbbb', png_chunk.data)
    return TimeChunk(png_chunk, time)


def get_bkgd_chunk(png_chunk: PngChunk, ihdr: IhdrChunk) -> Optional[BkgdChunk]:
    if png_chunk.chunk_name.name != b'bKGD':
        return None
    if ihdr.color_type in [ColorType.Grayscale, ColorType.GrayscaleAlpha]:
        background, *_ = unpack('>H', png_chunk.data)
    elif ihdr.color_type in [ColorType.Rgba, ColorType.Rgb]:
        r, g, b, *_ = unpack('>HHH', png_chunk.data)
        background = r, g, b
    else:
        background, *_ = unpack('b', png_chunk.data)
    return BkgdChunk(png_chunk, background)


def get_srgb_chunk(png_chunk: PngChunk) -> Optional[SrgbChunk]:
    if png_chunk.chunk_name.name != b'sRGB':
        return None
    ri, *_ = unpack('b', png_chunk.data)
    return SrgbChunk(png_chunk, RenderingIntent(ri))


def get_iend_chunk(png_chunk: PngChunk) -> Optional[IendChunk]:
    if png_chunk.chunk_name.name != b'IEND':
        return None
    check_many([png_chunk.length], ['IEND chunk length'], [lambda x: x == 0])
    return IendChunk(png_chunk)


def get_ihdr_chunk(png_chunk: PngChunk) -> Optional[IhdrChunk]:
    if png_chunk.chunk_name.name != b'IHDR':
        return None
    unpacked = unpack('>I I b b b b b', png_chunk.data)
    (width, height, bit_depth, color_type, compression_method,
     filter_method, interlace_method) = unpacked
    restrictions = [*map(contains, [[1, 2, 4, 8, 16], [0, 2, 3, 4, 6], [0], [0], [0, 1]])]
    names = ['width', 'height', 'bit depth', 'color type', 'compression method', 'filter method', 'interlace method']
    check_many(unpacked, names, [is_positive] * 2 + restrictions)
    return IhdrChunk(png_chunk, width, height, bit_depth, ColorType(color_type),
                     compression_method, FilterMethod(filter_method), InterlaceMethod(interlace_method))


def get_plte_chunk(png_chunk: PngChunk) -> Optional[PlteChunk]:
    if png_chunk.chunk_name.name != b'PLTE':
        return None
    check_many([png_chunk.length], ['PLTE chunk length'], [lambda x: not x % 3])
    palette = [unpack('bbb', png_chunk.data[i: i+3]) for i in range(0, png_chunk.length, 3)]
    return PlteChunk(png_chunk, tuple(palette))


def get_phys_chunk(png_chunk: PngChunk) -> Optional[PhysChunk]:
    if png_chunk.chunk_name.name != b'pHYs':
        return None
    ppu_x, ppu_y, unit = unpack('>IIb', png_chunk.data)
    return PhysChunk(png_chunk, (ppu_x, ppu_y), Unit(unit))


def read_chunks(source: ByteStream) -> Iterable[PngChunk]:
    sig = strict_read(source, 8)
    if sig != b'\x89PNG\r\n\x1a\n':
        raise PngFormatError(f'Invalid signature: {sig}')
    while 1:
        length_bytes = strict_read(source, 4, allow_empty=True)
        if not length_bytes:
            break
        length = unpack('>I', length_bytes)[0]
        chunk_name = ChunkName.from_bytes(strict_read(source, 4))
        data = strict_read(source, length)
        crc = unpack('>I', strict_read(source, 4))[0]
        yield PngChunk(length, chunk_name, data, crc)
