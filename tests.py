from assertpy import assert_that
from parameterized import parameterized
from png import read_png, get_pixels

RED = (255, 0, 0, 255)
GREEN = (0, 255, 0, 255)
TRANSPARENT = (0, 0, 0, 0)
AQUA = (0, 239, 255, 255)


@parameterized([
    'cases/3x2.png'
])
def test_get_pixels(filename):
    with open(filename, 'rb') as file:
        png = read_png(file)
        rgba = get_pixels(png)
        assert_that(rgba).is_equal_to((
            (RED, TRANSPARENT, AQUA),
            (TRANSPARENT, GREEN, TRANSPARENT),
        ))

