from tkinter import Tk, PhotoImage, Menu, Canvas, Text
from tkinter.filedialog import askopenfilename
from tkinter.ttk import Treeview
from tkinter.messagebox import showerror
from image_drawer import get_tk_image, get_grid
from png import read_png
from enum import Enum
from png_helper import PngChunk
from functools import wraps
from traceback import format_exc


def stringify_object(obj, depth=0):
    if isinstance(obj, Enum):
        return str(obj)
    if hasattr(obj, '__dict__'):
        fields = [f'{depth*" "*4}{x}: {stringify_object(y, depth+1)}'
                  for x, y in obj.__dict__.items()]
        fields.sort(key=len)
        return ('\n' if depth else '')+'\n'.join(fields)
    else:
        return str(obj)


def handle_error(func):
    @wraps(func)
    def wrapper(*args, **kwds):
        try:
            return func(*args, **kwds)
        except Exception as e:
            showerror('Error', f'{e}\n\n{format_exc()}')
    return wrapper


class PythonPngGui:
    def __init__(self):
        self.root = Tk()
        self.canvas = Canvas(self.root, width=400, height=400, bg='#ffffff')
        self.table = Treeview(self.root)
        self.menu = Menu(self.root)
        self.text = Text(self.root, width=390, height=200, font='Courier', state='disabled')
        self.img = PhotoImage(width=400, height=400)  # prevents gc removal
        self.chunks = []
        self.descriptions = []

    def initialize(self):
        self.root.resizable(width=False, height=False)
        self.root.title('python-png')
        self.root.geometry('820x420')
        self.table['columns'] = [1, 2, 3, 4]
        self.table.column('#0', width=35)
        self.table.column(1, width=60)
        self.table.column(2, width=110)
        self.table.column(3, width=120)
        self.table.column(4, width=60)
        self.table.heading('#0', text='№')
        self.table.heading(1, text='Chunk')
        self.table.heading(2, text='Length')
        self.table.heading(3, text='CRC')
        self.table.heading(4, text='Known')
        self.menu.add_command(label='Open .PNG', command=self.on_open_file)
        self.menu.add_command(label='Click on chunks for more information')
        self.menu.entryconfig('Click on chunks for more information', state='disabled')
        self.root.config(menu=self.menu)
        self.canvas.place(x=10, y=10, width=400, height=400)
        self.text.place(x=420, y=210, width=390, height=200)
        self.table.place(x=420, y=10, width=390, height=190)
        self.table.bind('<<TreeviewSelect>>', self.on_table_click)
        self.img = get_grid(400, 400)
        self.canvas.create_image((200, 200), image=self.img, state='normal')

    @handle_error
    def on_table_click(self, _):
        row_id = self.table.focus()
        if row_id:
            index = self.table.get_children().index(row_id)
            description = self.descriptions[index]
            self.set_text(description)

    @handle_error
    def on_open_file(self):
        name = askopenfilename(filetypes=[('PNG images', '*.png')])
        if name:
            with open(name, 'rb') as file:
                self.chunks = read_png(file).chunks
            self.descriptions = []
            self.root.title(f'python-png: {name}')
            self.clear_table()
            for chunk in self.chunks:
                chunk_name = repr(chunk.chunk_name.name)[2:-1]
                is_known = type(chunk) != PngChunk
                self.append_to_table((chunk_name, chunk.length, chunk.crc, is_known))
                self.descriptions.append(stringify_object(chunk))
            self.img = get_tk_image(name, 400, 400)
            self.canvas.create_image((200, 200), image=self.img, state='normal')

    def mainloop(self):
        self.root.mainloop()

    def clear_table(self):
        records = self.table.get_children()
        self.table.delete(*records)

    def append_to_table(self, values):
        index = len(self.table.get_children())
        self.table.insert('', 'end', text=index+1, values=values)

    def set_text(self, text):
        self.text.configure(state='normal')
        self.text.delete(1.0, 'end')
        self.text.insert('end', text)
        self.text.configure(state='disabled')


if __name__ == '__main__':
    gui = PythonPngGui()
    gui.initialize()
    gui.mainloop()
