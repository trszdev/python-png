from chunks import *
from png_helper import ByteStream, PngChunk, Any, PngFormatError
from typing import Tuple
from collections import defaultdict
from zlib import decompress
from numpy import array


class PngImage:
    def __init__(self, width: int, height: int, chunks: Tuple[PngChunk, ...]):
        self.width = width
        self.height = height
        self.chunks = chunks


def read_png(bs: ByteStream) -> PngImage:
    chunks = list(read_chunks(bs))
    ihdr = get_ihdr_chunk(chunks[0])
    if not isinstance(ihdr, IhdrChunk):
        raise PngFormatError('IHDR chunk expected to be first')
    chunks[0] = ihdr
    chunk_mappers = defaultdict(lambda: lambda x: x)
    chunk_mappers[b'IEND'] = get_iend_chunk
    chunk_mappers[b'PLTE'] = get_plte_chunk
    chunk_mappers[b'pHYs'] = get_phys_chunk
    chunk_mappers[b'sRGB'] = get_srgb_chunk
    chunk_mappers[b'bKGD'] = lambda x: get_bkgd_chunk(x, ihdr)
    chunk_mappers[b'tIME'] = get_time_chunk
    chunk_mappers[b'tRNS'] = lambda x: get_trns_chunk(x, ihdr)
    chunk_mappers[b'gAMA'] = get_gama_chunk
    for i, chunk in enumerate(chunks):
        chunks[i] = chunk_mappers[chunk.chunk_name.name](chunk)
    if not isinstance(chunks[-1], IendChunk):
        raise PngFormatError('IEND chunk expected to be last')
    return PngImage(ihdr.width, ihdr.height, tuple(chunks))


def get_pixels(png_image: PngImage) -> Tuple[Tuple[Any, ...], ...]:
    # it's not located in chunks.py
    # because it has more complex decoding method
    ihdr, *_ = [x for x in png_image.chunks if x.chunk_name.name == b'IHDR']
    if ihdr.interlace_method == InterlaceMethod.Adam7:
        raise NotImplementedError()
    idats = [x.data for x in png_image.chunks if x.chunk_name.name == b'IDAT']
    decompressed = decompress(b''.join(idats))
    pad = f'pad: {-((ihdr.bit_depth * ihdr.width) % -8)},'
    sample_pattern = f'uint: {ihdr.bit_depth},'
    multiplier = {
        ColorType.Rgb: 3,
        ColorType.Rgba: 4,
        ColorType.GrayscaleAlpha: 2
    }.get(ihdr.color_type, 1)
    sample_pattern *= multiplier
    row_pattern = 'pad: 8,' + sample_pattern * ihdr.width + pad
    pattern = row_pattern * ihdr.height
    dat = ConstBitArray(decompressed).unpack(pattern)
    result = array(dat).reshape((ihdr.height, ihdr.width, multiplier))
    result = tuple(tuple(tuple(rgba) for rgba in row) for row in result)
    return result
