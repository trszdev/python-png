# python-png

png decoding project
requires python3.6+ and some 3rd party libraries

![Screenshot](screenshot.png)

### quick start
install dependencies:
```cmd
python setup.py
```

then run gui viewer:
```cmd
python run_gui.py
```

or use in self code:
```python
from png import read_png, get_pixels, PngImage

with open('lenna.png', 'rb') as file:
    # chunk information stored here:
    png = read_png(file) # type: PngImage
    
    # pixel information, row by row:
    # don't forget about grayscale and indexed images
    pixels = get_pixels(png) 
```

### todo
adam7 interlace method isn't implemented, you are welcome to contribute :)