from tkinter import PhotoImage
from PIL import Image, ImageTk
from zlib import decompress
from base64 import b85decode
from io import BytesIO

GRID = 'c%17D@N?(olHy`uVBq!ia0y~yV4MKLOw2%$zZdVt11X*WpAgrZH*X#}a^yc4d`MaL1Srl}666=m;P' \
       'C858jy3-)5S5QV$R#M3wap~c~}lk_>%tV@Agk$7U&+$IkUz#^IYfd_&Y%j4hjMsEKH4XYVHL8r*9H' \
       '5go9PyKg-cX7oFgDVtLUw0}Nr!N#(`cM9@S9Jk*~ATUJY;3!e}<x!W?D3r&<`iSVbDb8LCgg;ONz)' \
       'bD<hFnADW0rVt;r>mdKI;Vst0JvvgF#'

GRID = Image.open(BytesIO(decompress(b85decode(GRID)))).convert('RGBA')


def get_tk_image(png_path: str, width: int, height: int) -> PhotoImage:
    bg = GRID.resize((width, height), Image.ANTIALIAS)
    width -= 20
    height -= 20
    image = Image.open(png_path).convert('RGBA')
    dim = min(width, height)
    aspect = image.width / image.height
    ndim = int(dim // aspect)
    if aspect > 1:
        image = image.resize((dim, ndim), Image.ANTIALIAS)
        offset = int((dim - ndim) // 2) + 10
        bg.paste(image, (10, offset), image)
    else:
        image = image.resize((int(dim * aspect), dim), Image.ANTIALIAS)
        ndim = int(dim * aspect)
        offset = int((dim - ndim) // 2) + 10
        bg.paste(image, (offset, 10), image)
    return ImageTk.PhotoImage(bg)


def get_grid(width: int, height: int) -> PhotoImage:
    bg = GRID.resize((width, height), Image.ANTIALIAS)
    return ImageTk.PhotoImage(bg)